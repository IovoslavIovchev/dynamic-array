use dynamic_array::array::Array;
use jemallocator::Jemalloc;
use serde::{Deserialize, Serialize};

// #[global_allocator]
// static GLOBAL: Jemalloc = Jemalloc;

fn main() {
    let mut arr = unsafe { Array::<char>::zeroed(5).assume_init() };

    for (i, c) in "Hello".char_indices() {
        arr[i] = c;
    }

    let arr2 = arr.clone();

    unsafe {
        let empty = Array::<char>::zeroed(0).assume_init();
        let e = empty.clone();
        assert!(empty.is_empty());
        dbg!(e);
    }

    let serialized = serde_json::to_string(&arr).unwrap();

    println!("{}", serialized);

    let deserialized: Array<char> = serde_json::from_str(&serialized).unwrap();

    dbg!(&deserialized);

    assert_eq!(deserialized.len(), 5);

    let dese_vec = deserialized.into_vec();
    dbg!(dese_vec);
}
