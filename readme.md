# dynamic-array

A dynamically-allocated array of fixed size.

```rust
use dynamic_array::SmallArray;

let mut arr = SmallArray::<u32>::zeroed(9);

assert!(!arr.is_empty());

// can be freely dereferenced
assert_eq!(arr[3], 0);

arr[7] = 8;

assert_eq!(arr[7], 8);

let mut arr2 = arr.clone();

assert_ne!(arr2[3],4);
arr[2] = 4;
arr2[3] = 4;
assert_eq!(arr[2],4);
assert_eq!(arr2[3],4);

// can also be freely iterated
for x in arr.iter_mut() {
    *x += 1;
}

assert_eq!(arr[2], 5);
```
