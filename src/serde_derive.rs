use crate::array::Array;

use serde::{
    de::{SeqAccess, Visitor},
    ser::{Serialize, Serializer},
    Deserialize, Deserializer,
};
use std::{
    convert::TryFrom,
    fmt::{self, Debug, Formatter},
    marker::PhantomData,
};

const DEFAULT_SIZE: usize = 1024;

impl<T: Serialize> Serialize for Array<T> {
    fn serialize<S: Serializer>(
        &self,
        serializer: S,
    ) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error> {
        serializer.collect_seq(self)
    }
}

impl<'de, T: Deserialize<'de>> Deserialize<'de> for Array<T> {
    fn deserialize<D: Deserializer<'de>>(
        deserializer: D,
    ) -> Result<Self, <D as Deserializer<'de>>::Error> {
        struct VecVisitor<T>(PhantomData<T>);

        impl<'de, T: Deserialize<'de>> Visitor<'de> for VecVisitor<T> {
            type Value = Array<T>;

            fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
                formatter.write_str("a sequence")
            }

            fn visit_seq<A: SeqAccess<'de>>(self, mut seq: A) -> Result<Self::Value, A::Error> {
                let size = std::cmp::min(seq.size_hint().unwrap_or(0), DEFAULT_SIZE);

                let mut vec = Vec::with_capacity(size);

                while let Some(el) = seq.next_element()? {
                    vec.push(el);
                }

                let arr = Array::from_vec(vec);

                Ok(arr)
            }
        }

        let v = VecVisitor(PhantomData);
        deserializer.deserialize_seq(v)
    }
}
