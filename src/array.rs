use std::{
    alloc::{alloc, alloc_zeroed, dealloc, Layout},
    cmp::{Eq, Ord, Ordering, PartialEq, PartialOrd},
    fmt,
    fmt::{Debug, Formatter},
    ops::{Deref, DerefMut, Index, IndexMut},
    ptr,
    slice::{from_raw_parts_mut, SliceIndex},
};

use crate::uninit::UninitArray;

/// A dynamically-allocated array of fixed size.
///
/// # Example
///
/// ```
/// use dynamic_array::Array;
///
/// let mut arr = unsafe {
///     let uninit_arr = Array::<u32>::zeroed(9);
///
///     // a zeroed or uninit array needs to be `assume_init`ed
///     uninit_arr.assume_init()
/// };
///
/// assert!(!arr.is_empty());
///
/// // can be freely dereferenced
/// assert_eq!(arr[3], 0);
///
/// arr[7] = 8;
///
/// assert_eq!(arr[7], 8);
///
/// let mut arr2 = arr.clone();
///
/// assert_ne!(arr2[3], 4);
/// arr[2] = 4;
/// arr[4] = 0xdead;
/// arr2[3] = 4;
/// assert_eq!(arr[2], 4);
///
/// // can be sliced
/// assert_eq!(arr[2..5], [4u32, 0, 0xdead]);
///
/// // can also be freely iterated
/// for x in arr.iter_mut() {
///     *x += 1;
/// }
///
/// assert_eq!(arr[2], 5);
///
/// ```
pub struct Array<T> {
    pub(crate) ptr: *mut T,
}

impl<T> Drop for Array<T> {
    fn drop(&mut self) {
        unsafe {
            if !self.ptr.is_null() {
                std::ptr::drop_in_place(self.deref_mut());
                let (lay, _) = Self::layout(self.len());
                dealloc(self.ptr.cast::<u8>(), lay)
            }
            //
            //Vec::from_raw_parts(self.ptr.as_ptr(), 0, self.len.into());
        }
    }
}

impl<T> Array<T> {
    pub(crate) fn layout(len: usize) -> (Layout, usize) {
        let usize_lay = Layout::new::<usize>();
        let t_lay = Layout::array::<T>(len).unwrap();

        usize_lay.extend(t_lay).unwrap()
    }
}

impl<T> Array<T> {
    fn empty() -> Self {
        Self {
            ptr: ptr::null_mut(),
        }
    }

    pub fn new_with<F>(len: usize, init_fn: F) -> Self
    where
        F: Fn(usize) -> T,
    {
        unsafe {
            let uninit = Self::uninit(len);
            let slice_mut = from_raw_parts_mut(uninit.as_mut_ptr(), len);

            for (i, el) in slice_mut.iter_mut().enumerate() {
                *el = init_fn(i);
            }

            uninit.assume_init()
        }
    }

    pub fn new_default(len: usize) -> Self
    where
        T: Default,
    {
        Self::new_with(len, |_| T::default())
    }

    /// Creates a new `Array<T>` with length `len` in an uninitialized state,
    /// with the buffer being filled with `0` bytes. It depends on `T` whether that already
    /// makes for proper initialization.
    ///
    /// For example, `Array<usize>::zeroed(5)` is already initialized,
    /// but `Array<&'static i32>::zeroed(8)` is not because references must not be null.
    ///
    /// Note that dropping the uninit array _will not_ call `T::drop()`. It is your responsibility to
    /// make sure `T` gets dropped if it got initialized.
    pub unsafe fn zeroed(len: usize) -> UninitArray<T> {
        let arr = if len == 0 {
            Self::empty()
        } else {
            let (layout, _) = Self::layout(len);

            let ptr = alloc_zeroed(layout).cast::<T>();
            *ptr.cast::<usize>() = len;

            Self { ptr }
        };

        UninitArray::new(arr)
    }

    /// Creates a new `Array<T>` with length `len` in an uninitialized state.
    ///
    /// Note that dropping the array _will not_ call `T::drop()`. It is your responsibility
    /// to make sure `T` gets dropped if it got initialized.
    pub unsafe fn uninit(len: usize) -> UninitArray<T> {
        let arr = if len == 0 {
            Self::empty()
        } else {
            let (layout, _) = Self::layout(len);

            let ptr = alloc(layout).cast::<T>();
            *ptr.cast::<usize>() = len;

            Self { ptr }
        };

        UninitArray::new(arr)
    }

    /// The length of the array
    ///
    /// # Example
    ///
    /// ```
    /// use dynamic_array::Array;
    /// let arr = unsafe { Array::<()>::uninit(42).assume_init() };
    ///
    /// assert_eq!(arr.len(), 42);
    /// ```
    pub fn len(&self) -> usize {
        if self.ptr.is_null() {
            0
        } else {
            unsafe { *self.ptr.cast::<usize>() }
        }
    }

    pub fn as_ptr(&self) -> *const T {
        self.as_mut_ptr() as *const T
    }

    pub fn as_mut_ptr(&self) -> *mut T {
        unsafe { self.ptr.cast::<usize>().add(1).cast::<T>() }
    }

    /// Extracts a slice containing all the elements of `self`.
    pub fn as_slice(&self) -> &[T] {
        self
    }

    /// Extracts a mutable slice containing all the elements of `self`.
    pub fn as_slice_mut(&mut self) -> &mut [T] {
        self
    }

    /// Constructs a `Vec` from `Self`.
    ///
    /// # Example
    ///
    /// ```
    /// use dynamic_array::Array;
    ///
    /// let arr = unsafe { Array::<usize>::zeroed(5).assume_init() };
    ///
    /// let v: Vec<usize> = arr.into_vec();
    ///
    /// assert_eq!(v.len(), 5);
    /// assert_eq!(v[3], 0);
    /// ```
    pub fn into_vec(self) -> Vec<T> {
        let size = self.len();
        let v = if size != 0 {
            unsafe {
                let v = Vec::from_raw_parts(self.as_mut_ptr(), size, size);

                // need to free the length part
                let usize_lay = Layout::new::<usize>();
                dealloc(self.ptr.cast::<u8>(), usize_lay);

                v
            }
        } else {
            Vec::new()
        };

        std::mem::forget(self);
        v
    }

    /// Constructs a new `Array` from a given `Vec`.
    ///
    /// # Example
    ///
    /// ```
    /// use dynamic_array::Array;
    ///
    /// let mut v = vec![8,9,10usize];
    /// v.reserve(1000);
    ///
    /// let arr = Array::from_vec(v);
    ///
    /// assert_eq!(arr.len(), 3);
    /// ```
    pub fn from_vec(v: Vec<T>) -> Self {
        unsafe {
            let len = v.len();
            let mut uninit = Self::uninit(len);
            let uninit_slice = uninit.as_slice_mut();

            for (el, uninit) in v.into_iter().zip(uninit_slice) {
                *uninit = el;
            }

            uninit.assume_init()
        }
    }
}

/**********************************************************************/
// Trait implementations

impl<T: Clone> Clone for Array<T> {
    fn clone(&self) -> Self {
        unsafe {
            let len = self.len();
            let other = Self::uninit(len);

            // If len == 0 this shouldn't cost anything
            other
                .as_mut_ptr()
                .copy_from_nonoverlapping(self.as_ptr(), len);

            other.assume_init()
        }
    }
}

impl<T> Deref for Array<T> {
    type Target = [T];

    fn deref(&self) -> &[T] {
        unsafe { std::slice::from_raw_parts(self.as_ptr(), self.len()) }
    }
}

impl<T> DerefMut for Array<T> {
    fn deref_mut(&mut self) -> &mut [T] {
        unsafe { std::slice::from_raw_parts_mut(self.as_mut_ptr(), self.len()) }
    }
}

impl<T: Debug> Debug for Array<T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(self.as_slice(), f)
    }
}

impl<T> IntoIterator for Array<T> {
    type Item = T;
    type IntoIter = std::vec::IntoIter<T>;

    fn into_iter(self) -> Self::IntoIter {
        self.into_vec().into_iter()
    }
}

impl<'t, T> IntoIterator for &'t Array<T> {
    type Item = &'t T;
    type IntoIter = std::slice::Iter<'t, T>;

    fn into_iter(self) -> std::slice::Iter<'t, T> {
        self.iter()
    }
}

impl<'t, T> IntoIterator for &'t mut Array<T> {
    type Item = &'t mut T;
    type IntoIter = std::slice::IterMut<'t, T>;

    fn into_iter(self) -> std::slice::IterMut<'t, T> {
        self.iter_mut()
    }
}

impl<T> PartialEq for Array<T>
where
    T: PartialEq<T>,
{
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self[..] == other[..]
    }
    #[inline]
    fn ne(&self, other: &Self) -> bool {
        self[..] != other[..]
    }
}

impl<T> Eq for Array<T> where T: Eq {}

impl<T, U> PartialEq<[U]> for Array<T>
where
    T: PartialEq<U>,
{
    #[inline]
    fn eq(&self, other: &[U]) -> bool {
        self[..] == other[..]
    }
    #[inline]
    fn ne(&self, other: &[U]) -> bool {
        self[..] != other[..]
    }
}

impl<T, U, const N: usize> PartialEq<[U; N]> for Array<T>
where
    T: PartialEq<U>,
{
    #[inline]
    fn eq(&self, other: &[U; N]) -> bool {
        self[..] == other[..]
    }
    #[inline]
    fn ne(&self, other: &[U; N]) -> bool {
        self[..] != other[..]
    }
}

impl<T> Ord for Array<T>
where
    T: Ord,
{
    #[inline]
    fn cmp(&self, other: &Self) -> Ordering {
        Ord::cmp(&**self, &**other)
    }
}

impl<T> PartialOrd for Array<T>
where
    T: Ord,
{
    #[inline]
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        PartialOrd::partial_cmp(&**self, &**other)
    }
}

impl<T, I> Index<I> for Array<T>
where
    I: SliceIndex<[T]>,
{
    type Output = I::Output;

    #[inline]
    fn index(&self, index: I) -> &Self::Output {
        Index::index(&**self, index)
    }
}

impl<T, I> IndexMut<I> for Array<T>
where
    I: SliceIndex<[T]>,
{
    fn index_mut(&mut self, index: I) -> &mut Self::Output {
        IndexMut::index_mut(&mut **self, index)
    }
}

/// `Default` does not always make sense for `Array`. This is an opt-in feature in case it is needed.
///
/// The resulting `Array` is rather unusable, as its length is 0.
#[cfg(feature = "default-derive")]
impl<T: Default> Default for Array<T> {
    fn default() -> Self {
        Self::empty()
    }
}

#[cfg(test)]
mod tests {
    use super::Array;

    #[cfg(feature = "default-derive")]
    #[test]
    fn default() {
        let arr = Array::<u8>::default();
        assert_eq!(arr.len(), 0);
    }
}
