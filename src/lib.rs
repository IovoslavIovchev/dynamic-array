pub mod array;
mod uninit;

#[cfg(feature = "serde-derive")]
pub mod serde_derive;

#[cfg(feature = "rkyv-derive")]
pub mod rkyv_derive;

#[cfg(feature = "serde-derive")]
#[doc(hidden)]
pub use serde_derive::*;

pub use array::Array;
