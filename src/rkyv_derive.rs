use crate::array::Array;

use rkyv::{
    ser::{ScratchSpace, Serializer},
    vec::{ArchivedVec, VecResolver},
    Archive, Deserialize, DeserializeUnsized, Fallible, Serialize,
};
use std::{alloc, convert::TryFrom, fmt::Debug};

impl<T: Archive, Len: Copy + Into<usize> + TryFrom<usize>> Archive for Array<T, Len> {
    type Archived = ArchivedVec<T::Archived>;
    type Resolver = VecResolver;

    #[inline]
    unsafe fn resolve(&self, pos: usize, resolver: Self::Resolver, out: *mut Self::Archived) {
        ArchivedVec::resolve_from_slice(self.as_slice(), pos, resolver, out);
    }
}

impl<
        T: Serialize<S>,
        S: ScratchSpace + Serializer + ?Sized,
        Len: Copy + Into<usize> + TryFrom<usize>,
    > Serialize<S> for Array<T, Len>
{
    #[inline]
    fn serialize(&self, serializer: &mut S) -> Result<Self::Resolver, S::Error> {
        ArchivedVec::<T::Archived>::serialize_from_slice(self.as_slice(), serializer)
    }
}

impl<T: Archive, D: Fallible + ?Sized, Len: Copy + Into<usize> + TryFrom<usize>>
    Deserialize<Array<T, Len>, D> for ArchivedVec<T::Archived>
where
    [T::Archived]: DeserializeUnsized<[T], D>,
    <Len as TryFrom<usize>>::Error: Debug,
{
    #[inline]
    fn deserialize(&self, deserializer: &mut D) -> Result<Array<T, Len>, D::Error> {
        unsafe {
            let data_address = self
                .as_slice()
                .deserialize_unsized(deserializer, |layout| alloc::alloc(layout))?;
            let metadata = self.as_slice().deserialize_metadata(deserializer)?;
            let ptr = ptr_meta::from_raw_parts_mut(data_address, metadata);
            let as_vec = Box::<[T]>::from_raw(ptr).into();
            Ok(Array::<_, _>::from_vec(as_vec))
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rkyv::{
        archived_root,
        ser::{serializers::AllocSerializer, Serializer},
        Infallible,
    };

    #[test]
    fn serialize_deserialize() {
        type A = Array<u32, usize>;
        let vec = vec![1, 4, 8, 15, 17];
        let array = A::from_vec(vec);

        let mut serializer = AllocSerializer::<256>::default();
        serializer.serialize_value(&array).unwrap();
        let bytes = serializer.into_serializer().into_inner();

        let archived = unsafe { archived_root::<A>(bytes.as_slice()) };
        assert_eq!(archived.len(), array.len());
        for i in 0..array.len() {
            assert_eq!(archived[i], array[i]);
        }

        let deserialized_array: A = archived.deserialize(&mut Infallible).unwrap();
        assert_eq!(deserialized_array.len(), array.len());
        for i in 0..array.len() {
            assert_eq!(deserialized_array[i], array[i]);
        }
    }
}
