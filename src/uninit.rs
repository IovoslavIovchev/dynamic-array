use std::mem::ManuallyDrop;

use crate::array::Array;

pub struct UninitArray<T>(pub(crate) ManuallyDrop<Array<T>>);

impl<T> UninitArray<T> {
    pub(crate) fn new(val: Array<T>) -> Self {
        Self(ManuallyDrop::new(val))
    }

    pub fn as_mut_ptr(&self) -> *mut T {
        self.0.as_mut_ptr()
    }

    pub unsafe fn as_slice_mut(&mut self) -> &mut [T] {
        self.0.as_slice_mut()
    }

    pub unsafe fn assume_init(self) -> Array<T> {
        ManuallyDrop::into_inner(self.0)
    }
}
